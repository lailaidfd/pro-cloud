/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.cache.aspectj;

import com.cloud.common.cache.annotation.CacheClear;
import com.cloud.common.cache.base.BaseCacheAspect;
import com.cloud.common.cache.redis.RedisDao;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * @Author Aijm
 * @Description 使用 aop 切面记录请求日志信息
 * @Date 2019/8/19
 */
@Aspect
@Component
@Order(10)
@Slf4j
public class CacheClearAspect extends BaseCacheAspect {

    @Autowired
    private RedisDao redisDao;


    @Around("@annotation(cacheClear)")
    public Object interceptor(ProceedingJoinPoint point, CacheClear cacheClear)
            throws Throwable {
        String key = getKey(point, cacheClear.scope(), cacheClear.key());
        if (!cacheClear.pattern()) {
            redisDao.delete(key);
        } else {
            redisDao.deletePattern(key);
        }
        return point.proceed();
    }


}