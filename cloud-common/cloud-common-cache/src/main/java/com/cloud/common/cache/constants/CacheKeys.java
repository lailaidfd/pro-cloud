/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.cache.constants;


import lombok.experimental.UtilityClass;

/**
 * cache 的key
 * @author Aijm
 * @since 2019/8/29
 */
@UtilityClass
public class CacheKeys {


    /**
     * 为null 时的字符串
     */
    public static final String EMPTY_OBJ = "${null}";

    /**
     * 为null 时的过期时间
     */
    public static final Long EXPIRETIME_NULL = 60L;


    /**
     * 用户缓存过期时间 默认 s 10小时
     */
    public static final Long DEFAULT_EXPIRETIME = 36000L;

}