/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.util;

import com.cloud.common.data.user.SystemService;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;


/**
 * 租户工具类
 * @author Aijm
 * @since 2020/4/6
 */
@UtilityClass
@Slf4j
public class SystemUtil {


    private static final SystemService systemService = SpringUtil.getBean(SystemService.class);


    /**
     *  登录后的租户id
     *   特别注意的是 oauth_client_details 表
     *   用该方法拿取不到租户Id 不过(在该处 该表不需要根据mybatisPlus查询用不上)
     * @return
     */
    public Integer getCurrentTenant() {
        return systemService.getUserTenantId();
    }

    /**
     * 获取当前用户id
     * @return
     */
    public Long getUserId() {
        return systemService.getUserId();
    }


    /**
     * 获取当前用户类型
     * @return
     */
    public Integer getUserType() {
        return systemService.getUserType();
    }


    /**
     * 获取当前用户名称
     * @return
     */
    public String getUserName() {
        return systemService.getUserName();
    }


}