/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.swagger.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Aijm
 * @Description swagger 配置
 * @Date 2019/12/30
 */
@Data
@ToString
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProps {

	/**
	 * 是否开启swagger
	 */
	private Boolean enabled;

	/**
	 * 作者
	 **/
	private String author;

	/**
	 * 标题
	 **/
	private String title;
	/**
	 * 描述
	 **/
	private String desc;
	/**
	 * 版本
	 **/
	private String version;

	/**
	 * 服务条款
	 **/
	private String termsOfServiceUrl;

	/**
	 * host信息
	 **/
	private String host;

	/**
	 * 授权地址
	 */
	private String authUri;



}