/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.beans.po.SysUser;
import com.cloud.common.data.base.IProService;


/**
 * 用户表
 *
 * @author Aijm
 * @date 2019-08-25 20:20:58
 */
public interface SysUserService extends IProService<SysUser> {

    /**
     * 分页查询
     * @param page
     * @param userDTO
     * @return
     */
    IPage<UserDTO> getSysUserPage(Page page, UserDTO userDTO);


    /**
     * 获取用户详细信息
     * @param userDTO
     * @return
     */
    boolean saveUserDTO(UserDTO userDTO);

    /**
     * 校验是否可以修改或者注册
     * @param userDTO
     * @return
     */
    boolean getCheckUserDTO(UserDTO userDTO);

    /**
     * 校验是否可以修改或者注册 某租户
     * @param userDTO
     * @return
     */
    boolean getCheckUserTenant(UserDTO userDTO);


    /**
     * 获取用户详细信息
     * @param userDTO
     * @return
     */
    boolean updateUserDTO(UserDTO userDTO);

    /**
     * 删除用户信息
     * @param id
     * @return
     */
    boolean removeUserDTO(Long id);

}