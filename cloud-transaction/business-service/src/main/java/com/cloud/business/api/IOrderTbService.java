/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.business.api;


import com.cloud.business.beans.po.OrderTb;
import com.cloud.common.util.base.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 *
 *
 * @author Aijm
 * @date 2020-03-15 18:34:59
 */
@FeignClient(value = "order-service")
public interface IOrderTbService {


    /**
     * 保存订单
     * @param orderTb
     * @return
     */
    @RequestMapping(value="/ordertb", method= RequestMethod.POST)
    public Result save(@RequestBody @Valid OrderTb orderTb);
}