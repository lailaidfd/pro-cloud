/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.storage.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.storage.beans.po.StorageTb;
import com.cloud.storage.service.StorageTbService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 *
 *
 * @author Aijm
 * @date 2020-03-15 18:17:15
 */
@RestController
@RequestMapping("/storagetb" )
@Api(value = "storagetb", tags = "storagetb管理")
public class StorageTbController {

    @Autowired
    private StorageTbService storageTbService;


    /**
     * 修改
     * @param storageTb
     * @return Result
     */
    @PutMapping
    public Result updateById(@RequestBody @Valid StorageTb storageTb) {
        StorageTb byId = storageTbService.getById(storageTb.getId());
        byId.setCount(byId.getCount()- storageTb.getCount());
        return Result.success(storageTbService.updateById(byId));
    }


}