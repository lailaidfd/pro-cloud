/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.generator.util;

import lombok.experimental.UtilityClass;

/**
 * 数据源相关常量
 * @author Aijm
 * @since 2019/6/16
 */
@UtilityClass
public class DataSourceConstants {
	/**
	 * 查询数据源的SQL
	 */
	public static final String QUERY_DATA_SOURCE = "select * from gen_data_source where del_flag = 0";

	/**
	 * 动态路由KEY
	 */
	public static final String DS_ROUTE_KEY = "id";

	/**
	 * 数据源名称
	 */
	public static final String DS_NAME = "name";

	/**
	 * jdbcurl
	 */
	public static final String DS_JDBC_URL = "url";

	/**
	 * 用户名
	 */
	public static final String DS_USER_NAME = "username";

	/**
	 * 密码
	 */
	public static final String DS_USER_PWD = "password";

}